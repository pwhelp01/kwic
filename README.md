# Programminging Language Paradigms (UG) Coursework 2015/2016 #
## Scala Assignment ##
Submitted by: Pete Whelpton (pwhelp01 / 12828513)

Lecturers: Keith Mannock / Trevor Fenner

Due date: 1st April 2016

N.b. This is an IntelliJ IDEA project.  Source files are located in the /src directory.  Application is command line, and file paths passed in need to be relative.  Ran out of time / motivation for the unit tests :(

References
----------
Alice in Wonderland, The Hound of the Baskervilles and Treasure Island extracts coutesey of Project Guttenberg : https://www.gutenberg.org

Source.fromFile().getLines.toList idea taken from Bruce Eckel's blog: http://www.artima.com/weblogs/viewpost.jsp?thread=328540

Whilst not actively using it as a reference whilst programming, I may have subconsciously remembered examples from Bruce Eckel's book (Seven Languages in Seven Weeks) too... so including it as a potential reference. 

The fold-a-list-into-a-map syntax was finally cracked thanks to the last post on this page: http://stackoverflow.com/questions/8016750/convert-list-of-tuple-to-map-and-deal-with-duplicate-key
