/**
  * Programminging Language Paradigms (UG) Coursework 2015/2016
  * Scala Assignment
  * Submitted by: Pete Whelpton (pwhelp01 / 12828513)
  * Lecturers: Keith Mannock / Trevor Fenner
  * Due date: 1st April 2016
  *
  * Unit Test Suite
  */
import org.scalatest.FunSuite

class SetSuite extends FunSuite {

  test("Get") {
    assert(Set.empty.size == 0)
  }

  test("Invoking head on an empty Set should produce NoSuchElementException") {
    intercept[NoSuchElementException] {
      Set.empty.head
    }
  }
}