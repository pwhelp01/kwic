
/**
  * Programminging Language Paradigms (UG) Coursework 2015/2016
  * Scala Assignment
  * Submitted by: Pete Whelpton (pwhelp01 / 12828513)
  * Lecturers: Keith Mannock / Trevor Fenner
  * Due date: 1st April 2016
  *
  *
  * Alice in Wonderland, The Hound of the Baskervilles and Treasure Island extracts coutesey of Project Guttenberg : https://www.gutenberg.org
  *
  * Source.fromFile().getLines.toList idea taken from Bruce Eckel's blog: http://www.artima.com/weblogs/viewpost.jsp?thread=328540
  * The fold-a-list-into-a-map syntax was finally cracked thanks to the last post on this page: http://stackoverflow.com/questions/8016750/convert-list-of-tuple-to-map-and-deal-with-duplicate-key
  */

import java.io.{File, FileOutputStream, PrintWriter}
import scala.collection.immutable.{ListMap, SortedMap, TreeMap}
import scala.io.Source

/**
  * Keyword in Context Application
  */
object Kwic extends App {

  /* 'Main' part of program */
  // Print greeting
  printGreeting()

  // Get stopword list and files to be processed list from user
  val stopwords = Source.fromFile("stop_words.txt").getLines.toList
  val fileList = getInput(None, List())

  // Process each individual file
  fileList.map{x => processFile(x, stopwords)}


  /**
    * Process an individual text file, creating the index and then writing it to a new text file
    *
    * @param file The file to be process
    * @param stopwords A list of stopwords to ignore during processing
    */
  def processFile(file: File, stopwords: List[String]): Unit = {

    // Inform user which file is being processed
    println(System.lineSeparator + "Processing: " + file.toString + System.lineSeparator)

    // Get all the lines from the file into a list, then process each line individually
    // N.b. zipWithIndex allows us to get the line number (awesome!) and flatmap means we get back a map, rather
    // than a list of maps..
    val lines: List[String] = Source.fromFile(file).getLines.toList
    val result = lines.filterNot(x => x.trim == "").zipWithIndex.flatMap{x => processLine(x._1, x._2, stopwords)}

    // Sort the results - easier to do here than inline above (i.e. didn't work when I tried!)
    val sortedResults = result.sortWith(_._1 < _._1)

    // Write the index to the file
    writeFile(file, sortedResults)
  }


  /**
    * Takes a filename and keyword index and writes them out to "kwic-index.txt"
    *
    * @param filename the name of the file being processed
    * @param index the KWIC index of the file being processed
    */
  def writeFile(filename: File, index: List[(String, List[Any])]): Unit = {

    // Create a new Java IO PrintWriter object in Append mode (couldn't find a simple equivilent in Scala libraries?!)
    val pw = new PrintWriter(new FileOutputStream(new File("kwic-index.txt"),true));

    // Wrtie the name of the file being processed and underline it to make it stand out
    pw.write(filename.toString + System.lineSeparator)
    pw.write("===========================" + System.lineSeparator)

    // Write to index to the file in the format specified in the spec.  Assume that the page number will never go over 4 digits
    index.map(x => pw.write("%-4d".format(x._2(0)) + " " + "%30s".format(x._2(1)) + "  " + x._1 + "%-30s".format(x._2(2)) + System.lineSeparator))

    // Write a newline at the end of the index file (ready for the next file being processed), close the printwriter to make the buffer write to disk
    pw.write(System.lineSeparator)
    pw.close
  }


  /**
    * Process an indvidual line of a text file, spliting it into a map containing the keyword and metadata
    *
    * @param line The line to be processed
    * @param lineNo The line number of the line being processed
    * @param stopwords A list of stopwords to exclude to processing the line
    * @return A map contating [keyword, (linenumber, 30 chars before keyword, 30 chars after keyword)]
    */
  def processLine(line: String, lineNo: Int, stopwords: List[String]): Map[String, List[Any]] = {

    // Regular expession of characters we want to include in keywords (letters and apostrophes)
    val regex = "[a-zA-Z \']".r

    // Make a line including only lowercase a-Z and apostophe, then filter out stop-words
    val cleanLine = regex.findAllIn(line).mkString.toLowerCase
    val keywords: List[String] = cleanLine.split(" +").filterNot(x => stopwords.contains(x)).toList

    // Return a map contating keyword, (linenumber, 30 chars before keyword, 30 chars after keyword)
    keywords.filter(x => line.contains(x))
                      .foldLeft(Map.empty[String, List[Any]]){(m, s) => m.updated(s, List(lineNo, line.slice(line.toLowerCase.indexOfSlice(s) - 30, line.toLowerCase.indexOfSlice(s)), line.slice(line.toLowerCase.indexOfSlice(s) + s.length, line.toLowerCase.indexOfSlice(s) + s.length + 30)))}
  }


  /**
    * Get a list of filenames (recursively!) from the console.
    * Check if the file exists, and if so add it to the list of files.
    * Quit when an empty string ("") is returned.
    *
    * @param input The last file location input by the user
    * @param files Cumulative list of files input by user
    * @return Completed list of files input by user
    */
  def getInput(input: Option[String], files:List[File]): List[File] = {

    input match {
      // User input an empty string, so return list of files collated thus far
      case Some("") => { files }

      // User does not want to quit, so process another filename
      case _ => {

      // Prompt for user input, store in constant line and create a new Java file object from the path
      print (System.lineSeparator + "Please enter a relative path to a text (ACSII/UTF) file to process, or press [Return] if finished: ")
      val line = scala.io.StdIn.readLine ()
      val file = new File (line)

      // Check if user wants to end routine
      if (line == "") {
      getInput (Option (line), files)
      }
      else if (file.exists () && !file.isDirectory) {
      println (file + " will be processed.")
      getInput (Option (line), files :+ file)
      }
      // File doesnt exist, so warn user and prompt for a new file name
      else {
      println (line + " cannot be found.  Please check path and try again.")
      getInput (Option (line), files)
      }
    }
    }
  }

  /**
    * Print to greeting to the user
    */
  def printGreeting(): Unit = {

    println("Keyword In Context Indexer")
    println("Programminging Language Paradigms (UG) Coursework 2015/2016 - Pete Whelpton")
    println("===========================================================================" + System.lineSeparator)

  }
}